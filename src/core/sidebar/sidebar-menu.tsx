/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';

import {DecimeState} from '../../redux-saga/decime-reducer';
import {View, StyleSheet, Text, Alert} from 'react-native';
import {connect} from 'react-redux';
import {LOGOUT_ACTION_TYPE} from '../../redux-saga/actions/login-action';

interface Props {
  navigation: any;
  dispatch(action: any): void;
  decimeState: DecimeState;
}

interface State {
  items: any[];
}

class CustomSidebarMenu extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      items: [
        {
          navOptionName: 'Lista problemów',
          screenToNavigate: 'HomeScreen',
        },
        {
          navOptionName: 'Dodaj problem',
          screenToNavigate: 'AddProblemScreen',
        },
        {
          navOptionName: 'Wyloguj się',
          screenToNavigate: 'logout',
        },
      ],
    };
  }

  handleClick = (index: number, screenToNavigate: string) => {
    if (screenToNavigate === 'logout') {
      this.props.navigation.toggleDrawer();
      Alert.alert(
        'Wyloguj się',
        'Czy napewno chcesz się wylogować?',
        [
          {
            text: 'Nie',
            onPress: () => {
              return null;
            },
          },
          {
            text: 'Tak',
            onPress: () => {
              this.props.dispatch({
                type: LOGOUT_ACTION_TYPE,
              });
              this.props.navigation.navigate('Auth');

              console.log('logout');
            },
          },
        ],
        {cancelable: false},
      );
    } else {
      this.props.navigation.toggleDrawer();
      (global as any).currentScreenIndex = screenToNavigate;
      this.props.navigation.navigate(screenToNavigate);
    }
  };

  render() {
    return (
      <View style={stylesSidebar.sideMenuContainer}>
        <View style={stylesSidebar.profileHeader}>
          <View style={stylesSidebar.profileHeaderPicCircle}>
            <Text style={{fontSize: 25, color: '#307ecc'}}>
              {this.props.decimeState.auth.userName!.toUpperCase().charAt(0)}
            </Text>
          </View>
          <Text style={stylesSidebar.profileHeaderText}>
            {this.props.decimeState.auth.userName}
          </Text>
        </View>
        <View style={stylesSidebar.profileHeaderLine} />
        <View style={{width: '100%', flex: 1}}>
          {this.state.items.map((item, key) => (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                padding: 20,
                color: 'white',
                backgroundColor:
                  global.currentScreenIndex === item.screenToNavigate
                    ? '#4b9ff2'
                    : '#307ecc',
              }}
              key={key}
              onStartShouldSetResponder={() =>
                this.handleClick(key, item.screenToNavigate)
              }>
              <Text style={{fontSize: 15, color: 'white'}}>
                {item.navOptionName}
              </Text>
            </View>
          ))}
        </View>
      </View>
    );
  }
}

const stylesSidebar = StyleSheet.create({
  sideMenuContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: '#307ecc',
    paddingTop: 40,
    color: 'white',
  },
  profileHeader: {
    flexDirection: 'row',
    backgroundColor: '#307ecc',
    padding: 15,
    textAlign: 'center',
  },
  profileHeaderPicCircle: {
    width: 60,
    height: 60,
    borderRadius: 60 / 2,
    color: 'white',
    backgroundColor: '#ffffff',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  profileHeaderText: {
    color: 'white',
    alignSelf: 'center',
    paddingHorizontal: 10,
    fontWeight: 'bold',
  },
  profileHeaderLine: {
    height: 1,
    marginHorizontal: 20,
    backgroundColor: '#e2e2e2',
    marginTop: 15,
    marginBottom: 10,
  },
});
const mapStateToProps = (state: any) => {
  const {decimeState} = state;
  return {decimeState};
};

export default connect(mapStateToProps)(CustomSidebarMenu);
