/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  ScrollView,
  TextInput,
  Text,
  KeyboardAvoidingView,
  View,
  Dimensions,
  StyleSheet,
  FlatList,
  Alert,
  TouchableHighlight,
  Button,
} from 'react-native';
import {connect} from 'react-redux';
import {DecimeState} from '../../redux-saga/decime-reducer';
import axios, {AxiosResponse} from 'axios';
import {Picker} from '@react-native-community/picker';
import {ADD_PROBLEM_URL, GET_PROBLEMS_CATEGORIES} from '../../api';
import {Icon} from 'react-native-elements';

const windowWidth = Dimensions.get('window').width;

interface Props {
  navigation: any;
  dispatch(action: any): void;
  decimeState: DecimeState;
}

interface State {
  problemQuestion: string;
  possibleAnswers: string[];
  creditsAmount: number | null;
  selectedCategory: string;
  categories: string[];
  tempAnswer: string;
}

class AddProblemScreen extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      problemQuestion: '',
      possibleAnswers: [],
      selectedCategory: 'inne',
      categories: ['inne'],
      tempAnswer: '',
      creditsAmount: null,
    };
  }

  componentDidMount() {
    this.fetchCategories();
  }

  fetchCategories = () => {
    axios
      .get(GET_PROBLEMS_CATEGORIES, {
        headers: {Authorization: this.props.decimeState.auth.token},
      })
      .then((response: AxiosResponse) => {
        this.setState({
          categories: response.data,
        });
      })
      .catch((_err) => {
        Alert.alert('Wystąpił nieoczekiwany błąd');
      });
  };

  addProblem = () => {
    axios
      .post(
        ADD_PROBLEM_URL,
        {
          problemQuestion: this.state.problemQuestion,
          possibleAnswers: this.state.possibleAnswers,
          creditsAmount: this.state.creditsAmount,
          category: this.state.selectedCategory,
        },
        {
          headers: {Authorization: this.props.decimeState.auth.token},
        },
      )
      .then((_response: AxiosResponse) => {
        Alert.alert('Sukces!');
        this.setState({
          problemQuestion: '',
          possibleAnswers: [],
          tempAnswer: '',
          creditsAmount: null,
        });
      })
      .catch((_err) => {
        Alert.alert('Wystąpił nieoczekiwany błąd!');
      });
  };

  deleteTask = (index: number) => {
    const answers: string[] = this.state.possibleAnswers;
    this.setState({
      possibleAnswers: [
        ...answers.slice(0, index),
        ...answers.slice(index + 1),
      ],
    });
  };

  render() {
    return (
      <ScrollView style={styles.mainBox} keyboardShouldPersistTaps="handled">
        <View style={{marginTop: 10}}>
          <KeyboardAvoidingView enabled>
            <View style={{alignItems: 'center'}}>
              <Text style={{fontSize: 20}}>Wybrana kategoria</Text>
            </View>
            <Picker
              style={{width: windowWidth * 0.7, alignSelf: 'center'}}
              selectedValue={this.state.selectedCategory}
              onValueChange={(value, _) => {
                this.setState({
                  selectedCategory: value.toString(),
                });
              }}>
              {this.state.categories.map((category) => (
                <Picker.Item key={category} label={category} value={category} />
              ))}
            </Picker>
            <View style={{alignItems: 'center'}}>
              <Text style={{fontSize: 20}}>Twoje pytanie</Text>
              <TextInput
                style={styles.inputStyle}
                value={this.state.problemQuestion}
                onChangeText={(problemQuestion: string) =>
                  this.setState({problemQuestion: problemQuestion})
                }
                underlineColorAndroid="#FFFFFF"
                placeholder="Wprowadź pytanie"
                autoCapitalize="none"
                returnKeyType="next"
                blurOnSubmit={false}
              />
              <Text style={{fontSize: 20, marginTop: 10}}>
                Twoje odpowiedzi
              </Text>
              <View
                style={{
                  marginTop: 10,
                  alignItems: 'flex-start',
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                }}>
                <TextInput
                  style={styles.tempAnswer}
                  value={this.state.tempAnswer}
                  onChangeText={(tempAnswer: string) =>
                    this.setState({tempAnswer: tempAnswer})
                  }
                  underlineColorAndroid="#FFFFFF"
                  placeholder="Wprowadź odpowiedz"
                  autoCapitalize="none"
                  returnKeyType="next"
                  blurOnSubmit={false}
                />
                <TouchableHighlight
                  style={{
                    backgroundColor: '#307ecc',
                    height: 55,
                    width: 55,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  onPress={() => {
                    console.log('clicked');
                    if (this.state.tempAnswer.trim() !== '') {
                      let answers: string[] = this.state.possibleAnswers;
                      if (this.state.tempAnswer in answers) {
                        Alert.alert('Ta odpowedz została już doadana.');
                      } else {
                        answers.push(this.state.tempAnswer);
                        this.setState({tempAnswer: ''});
                        this.setState({
                          possibleAnswers: answers,
                        });
                      }
                    } else {
                      Alert.alert('Odpowiedz nie może być pusta');
                    }
                  }}>
                  <View>
                    <Icon
                      name="arrow-circle-right"
                      type="font-awesome"
                      color="white"
                    />
                  </View>
                </TouchableHighlight>
              </View>
              <FlatList
                style={styles.list}
                data={this.state.possibleAnswers}
                keyExtractor={(item, index) => item + index}
                renderItem={({item, index}) => (
                  <View>
                    <View style={styles.listItemCont}>
                      <Text style={styles.listItem}>{item}</Text>
                      <Button
                        title="X"
                        onPress={() => this.deleteTask(index)}
                      />
                    </View>
                    <View style={styles.hr} />
                  </View>
                )}
              />
              <Button
                title="Dodaj pytanie"
                onPress={() => {
                  if (this.state.possibleAnswers.length < 2) {
                    Alert.alert('Liczba odpowiedzi musi być większa od 1');
                  } else {
                    if (this.state.problemQuestion.trim() !== '') {
                      this.addProblem();
                    } else {
                      Alert.alert('Pole z pytaniem nie może byc puste');
                    }
                  }
                }}
              />
            </View>
          </KeyboardAvoidingView>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  inputStyle: {
    flex: 1,
    marginTop: 10,
    paddingLeft: 15,
    paddingRight: 14,
    borderWidth: 1,
    width: windowWidth - 40,
  },
  tempAnswer: {
    alignSelf: 'flex-start',
    flexDirection: 'column',
    marginBottom: 20,
    paddingLeft: 15,
    paddingRight: 14,
    borderWidth: 1,
    width: windowWidth - 150,
    height: 55,
  },
  listItemCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  mainBox: {
    flex: 1,
  },
  list: {
    width: windowWidth - 40,
    marginBottom: 20,
  },
  listItem: {
    paddingTop: 2,
    paddingBottom: 2,
    fontSize: 18,
  },
  hr: {
    height: 1,
    backgroundColor: 'gray',
  },
});

const mapStateToProps = (state: any) => {
  const {decimeState} = state;
  return {decimeState};
};

export default connect(mapStateToProps)(AddProblemScreen);
