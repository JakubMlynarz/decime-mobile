/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {Picker} from '@react-native-community/picker';
import {DecimeState} from '../../redux-saga/decime-reducer';
import {connect} from 'react-redux';
import {Problem} from '../../model/problem';
import Moment from 'moment';
import {SearchBar} from 'react-native-elements';
import axios, {AxiosResponse} from 'axios';
import {GET_PROBLEMS_CATEGORIES, GET_PROBLEMS_IN_CATEGORY} from '../../api';

const globalAny: any = global;

interface ContentParams {
  layoutMeasurement: any;
  contentOffset: any;
  contentSize: any;
}

const isCloseToBottom = (cp: ContentParams) => {
  const paddingToBottom = 20;
  return (
    cp.layoutMeasurement.height + cp.contentOffset.y >=
    cp.contentSize.height - paddingToBottom
  );
};

interface Props {
  navigation: any;
  dispatch(action: any): void;
  decimeState: DecimeState;
}

interface State {
  search: string;
  selectedCategory: string;
  categories: string[];
  problems: Problem[];
}

class HomeScreen extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      search: '',
      selectedCategory: 'inne',
      problems: [],
      categories: ['inne'],
    };
  }

  componentDidMount() {
    this.fetchProblems(this.state.selectedCategory);
    this.fetchCategories();
  }

  getProblems = () => {
    if (this.state.search === '') {
      return this.state.problems;
    } else {
      return this.state.problems.filter((problem: Problem) =>
        problem.problemQuestion
          ?.toLowerCase()
          .includes(this.state.search.toLowerCase()),
      );
    }
  };

  fetchProblems = (category: string) => {
    axios
      .get(GET_PROBLEMS_IN_CATEGORY.replace('{category}', category), {
        headers: {Authorization: this.props.decimeState.auth.token},
      })
      .then((response: AxiosResponse) => {
        this.setState({
          problems: response.data,
        });
      })
      .catch((_err) => {
        console.log(_err);
        Alert.alert('Wystąpił nieoczekiwany błąd: ');
      });
  };

  fetchCategories = () => {
    axios
      .get(GET_PROBLEMS_CATEGORIES, {
        headers: {Authorization: this.props.decimeState.auth.token},
      })
      .then((response: AxiosResponse) => {
        this.setState({
          categories: response.data,
        });
      })
      .catch((_err) => {
        Alert.alert('Wystąpił nieoczekiwany błąd');
      });
  };

  render() {
    globalAny.currentScreenIndex = 'HomeScreen';
    return (
      <ScrollView
        style={{flex: 1, marginTop: 0}}
        onScroll={({nativeEvent}) => {
          if (isCloseToBottom(nativeEvent)) {
            this.fetchProblems(this.state.selectedCategory);
          }
        }}>
        <Picker
          selectedValue={this.state.selectedCategory}
          onValueChange={(value, _) => {
            this.setState({
              selectedCategory: value.toString(),
            });
            this.fetchProblems(value.toString());
          }}>
          {this.state.categories.map((category) => (
            <Picker.Item key={category} label={category} value={category} />
          ))}
        </Picker>
        <SearchBar
          placeholder="Szukaj..."
          onChangeText={(text: string) =>
            this.setState({
              search: text.trim(),
            })
          }
          value={this.state.search}
        />
        {this.getProblems().map((problem: Problem, index: number) => (
          <View
            key={index}
            style={[
              problem.promotionAgreement !== null
                ? styles.problemBoxExtra
                : styles.problemBox,
            ]}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('Second', {id: problem.id});
              }}>
              <Text style={styles.question}>{problem.problemQuestion}</Text>
              <Text style={styles.problemMetaInfo}>
                Dodano{' '}
                {Moment(problem.creationDate).format('MMMM Do, YYYY H:mma')}
              </Text>
              <Text style={styles.problemMetaInfo}>
                {problem.possibleAnswers
                  .map((pa) => pa.count)
                  .reduce((a, b) => a + b)}{' '}
                odpowiedzi
              </Text>
              <View style={styles.divider} />
            </TouchableOpacity>
          </View>
        ))}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  problemBox: {
    flex: 1,
    marginLeft: 30,
    marginRight: 30,
  },
  problemBoxExtra: {
    flex: 1,
    marginLeft: 30,
    marginRight: 30,
    backgroundColor: 'yellow',
  },
  problemMetaInfo: {
    fontSize: 12,
    marginTop: 5,
    marginBottom: 5,
    textAlign: 'right',
  },
  question: {
    textAlign: 'center',
    fontSize: 18,
    marginTop: 10,
    marginBottom: 5,
    color: 'red',
  },
  divider: {
    marginTop: 5,
    borderBottomColor: 'blue',
    borderBottomWidth: 1,
  },
});

const mapStateToProps = (state: any) => {
  const {decimeState} = state;
  return {decimeState};
};

export default connect(mapStateToProps)(HomeScreen);
