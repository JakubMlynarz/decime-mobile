/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Text, StyleSheet, ScrollView, Alert} from 'react-native';
import {DecimeState} from '../../redux-saga/decime-reducer';
import {connect} from 'react-redux';
import {Problem, PossibleAnswer} from '../../model/problem';
import Moment from 'moment';
import Loader from '../../login-register/loader/loader';
import {
  GET_PROBLEM_URL,
  VOTE_PROBLEM_URL,
  GET_COMMENTS_FOR_PROBLEM,
  ADD_COMMENT,
} from '../../api';
import axios, {AxiosResponse} from 'axios';
import Comments from 'react-native-comments';
import RNPoll, {IChoice} from 'react-native-poll';

const globalAny: any = global;

interface Props {
  navigation: any;
  dispatch(action: any): void;
  decimeState: DecimeState;
}

interface Comment {
  id: number;
  responseTo: number | null;
  userNick: string;
  commentValue: string;
  createdAt: string | null;
  children: Comment[];
}

interface CommentDto {
  id: number;
  responseTo: number | null;
  userNick: string;
  commentValue: string;
  createdAt: string | null;
}

interface State {
  id: number;
  selectedAnswer: string;
  comments: Comment[];
  problem: Problem | null;
}

class ProblemDetails extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      id: this.props.navigation.getParam('id'),
      comments: [],
      selectedAnswer: '0',
      problem: null,
    };
  }

  componentDidMount() {
    this.fetchProblem();
    this.fetchComments();
  }

  fetchComments = () => {
    axios
      .get(GET_COMMENTS_FOR_PROBLEM.replace('{id}', this.state.id.toString()), {
        headers: {Authorization: this.props.decimeState.auth.token},
      })
      .then((response: AxiosResponse<CommentDto[]>) => {
        this.setState({
          comments: response.data
            .map((commentDto: CommentDto) =>
              this.mapToComments(commentDto, response.data),
            )
            .filter((comment: Comment) => comment.responseTo === null),
        });
        console.log(this.state.comments);
      })
      .catch((err) => {
        console.log(err);
        Alert.alert('Unexpected error occured');
      });
  };

  mapToComments = (comment: CommentDto, allComments: CommentDto[]): Comment => {
    return {
      id: comment.id,
      responseTo: comment.responseTo,
      userNick: comment.userNick,
      commentValue: comment.commentValue,
      createdAt: comment.createdAt,
      children: allComments
        .filter(
          (commentDto: CommentDto) => commentDto.responseTo === comment.id,
        )
        .map((commentDto: CommentDto) =>
          this.mapToComments(commentDto, allComments),
        ),
    };
  };

  fetchProblem = () => {
    axios
      .get(GET_PROBLEM_URL.replace('{id}', this.state.id.toString()), {
        headers: {Authorization: this.props.decimeState.auth.token},
      })
      .then((response: AxiosResponse<Problem>) => {
        this.setState({
          problem: response.data,
          selectedAnswer: response.data.possibleAnswers[1].id.toString(),
        });
      })
      .catch((err) => {
        console.log(err);
        Alert.alert('Unexpected error occured');
        this.props.navigation.navigate('First');
      });
  };

  voteProblem = (id: number) => {
    axios
      .post(
        VOTE_PROBLEM_URL,
        {
          possibleAnswerId: id,
          comment: null,
        },
        {
          headers: {
            Authorization: this.props.decimeState.auth.token,
          },
        },
      )
      .then((_response: AxiosResponse) => {
        this.fetchProblem();
      })
      .catch((err) => {
        console.log(err);
        Alert.alert('Już raz odpowiedziałeś na to pytanie!');
      });
  };
  getAllVotesCount = (problem: Problem): number => {
    let votes: number[] = problem.possibleAnswers.map(
      (pa: PossibleAnswer) => pa.count,
    );
    return votes.reduce((a, b) => a + b);
  };

  getUserVote = (problem: Problem): string | null => {
    for (let pa of problem.possibleAnswers) {
      for (let pi of pa.answerInstances) {
        if (pi.userNick === this.props.decimeState.auth.userName) {
          return pa.answer;
        }
      }
    }
    return null;
  };

  addComment = (value: string, parent: number | null) => {
    let parentId: number | null = null;
    if (isNaN(Number(parent))) {
      parentId = parent;
    }
    axios
      .post(
        ADD_COMMENT,
        {
          comment: value,
          responseTo: parentId,
          decisionProblemId: this.state.id,
        },
        {
          headers: {
            Authorization: this.props.decimeState.auth.token,
          },
        },
      )
      .then((_response: AxiosResponse) => {
        this.fetchComments();
      })
      .catch((err) => {
        console.log(err);
        Alert.alert(err);
      });
  };

  possibleAnswerToChoice = (pa: PossibleAnswer): IChoice => {
    return {
      id: pa.id,
      votes: pa.count,
      choice: pa.answer,
    };
  };

  getAnswersData = (problem: Problem): IChoice[] => {
    return problem.possibleAnswers.map(this.possibleAnswerToChoice);
  };

  render() {
    globalAny.currentScreenIndex = 'HomeScreen';
    if (this.state.problem !== null) {
      return (
        <ScrollView style={{flex: 1, marginTop: 10}}>
          <View style={styles.problemBox}>
            <Text style={styles.question}>
              {this.state.problem?.problemQuestion}
            </Text>
            <Text style={styles.problemMetaInfo}>
              Dodano{' '}
              {Moment(this.state.problem?.creationDate).format(
                'MMMM Do, YYYY H:mma',
              )}
            </Text>
            <RNPoll
              hasBeenVoted={this.getUserVote(this.state.problem) !== null}
              totalVotes={this.getAllVotesCount(this.state.problem)}
              choices={this.getAnswersData(this.state.problem)}
              onChoicePress={(selectedChoice: IChoice) => {
                console.log('SelectedChoice: ', selectedChoice);
                this.voteProblem(selectedChoice.id);
              }}
            />
          </View>
          <View style={styles.commentBox}>
            <Comments
              data={this.state.comments}
              styles={{}}
              viewingUserName={this.props.decimeState.auth.userName}
              userIsAdmin={false}
              childPropName={'children'}
              initialDisplayCount={5}
              keyExtractor={(item: Comment) => item.id}
              bodyExtractor={(item: Comment) => item.commentValue}
              usernameExtractor={(item: Comment) => item.userNick}
              likeExtractor={(_item: Comment) => false}
              parentIdExtractor={(item: Comment) => item.responseTo}
              likesExtractor={(_item: Comment) => []}
              editTimeExtractor={(item: Comment) => item.createdAt}
              createdTimeExtractor={(item: Comment) => item.createdAt}
              imageExtractor={(_item: Comment) => null}
              isChild={(item: Comment) => item.responseTo !== null}
              childrenCountExtractor={(item: Comment) => item.children.length}
              //replyAction={(__do_nothing: any) => {}}
              saveAction={(text: any, parentCommentId: any) => {
                console.log(text);
                this.addComment(text, parentCommentId);
              }}
              editAction={(_text: any, _comment: any) => {}}
              deleteAction={(_comment: any) => {}}
            />
          </View>
        </ScrollView>
      );
    } else {
      return (
        <View>
          <Loader loading={this.state.problem === null} />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  problemBox: {
    flex: 1,
    marginLeft: 30,
    marginRight: 30,
  },
  commentBox: {
    flex: 1,
    marginLeft: 5,
    marginRight: 5,
  },
  problemMetaInfo: {
    fontSize: 12,
    marginTop: 5,
    marginBottom: 5,
    textAlign: 'right',
  },
  question: {
    textAlign: 'center',
    fontSize: 18,
    marginTop: 10,
    marginBottom: 5,
    color: 'red',
  },
  voteHeader: {
    textAlign: 'center',
    fontSize: 18,
    marginTop: 10,
    marginBottom: 5,
    color: 'blue',
  },
});

const mapStateToProps = (state: any) => {
  const {decimeState} = state;
  return {decimeState};
};

export default connect(mapStateToProps)(ProblemDetails);
