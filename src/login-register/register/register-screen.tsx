/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';

import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  KeyboardAvoidingView,
  Keyboard,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Loader from '../loader/loader';
import {DecimeState} from '../../redux-saga/decime-reducer';
import axios, {AxiosResponse} from 'axios';
import {REGISTER_URL} from '../../api';

interface Props {
  navigation: any;
  dispatch(action: any): void;
  decimeState: DecimeState;
}

interface State {
  nick: string;
  userEmail: string;
  userPassword: string;
  userPassword2: string;
  loading: boolean;
  errorText: string;
  registered: boolean;
}

class RegisterScreen extends Component<Props, State> {
  emailInput: TextInput | undefined;
  password1Input: TextInput | undefined;
  password2Input: TextInput | undefined;
  constructor(props: Props) {
    super(props);
    console.log(props);
    this.state = {
      nick: '',
      userEmail: '',
      userPassword: '',
      userPassword2: '',
      loading: false,
      registered: false,
      errorText: '',
    };
  }

  render() {
    if (this.state.registered) {
      return (
        <View
          style={{
            flex: 1,
            backgroundColor: '#307ecc',
            justifyContent: 'center',
          }}>
          <Image
            source={require('../images/success.png')}
            style={{height: 150, resizeMode: 'contain', alignSelf: 'center'}}
          />
          <Text style={styles.successTextStyle}>Zarejestrowano pomyślnie.</Text>
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={() => {
              this.props.navigation.navigate('LoginScreen');
            }}>
            <Text style={styles.buttonTextStyle}>
              Masz już konto ? Zaloguj się
            </Text>
          </TouchableOpacity>
        </View>
      );
    }
    return (
      <View style={{flex: 1, backgroundColor: '#307ecc'}}>
        <Loader loading={this.state.loading} />
        <ScrollView keyboardShouldPersistTaps="handled">
          <View style={{alignItems: 'center'}}>
            <Image
              source={require('../images/react.png')}
              style={{
                width: '50%',
                height: 100,
                resizeMode: 'contain',
                margin: 30,
              }}
            />
          </View>
          <KeyboardAvoidingView enabled>
            <View style={styles.SectionStyle}>
              <TextInput
                value={this.state.nick}
                style={styles.inputStyle}
                onChangeText={(nick: string) =>
                  this.setState({nick: nick.trim()})
                }
                underlineColorAndroid="#FFFFFF"
                placeholder="Wprowadź nazwę użytkownika"
                placeholderTextColor="#F6F6F7"
                returnKeyType="next"
                onSubmitEditing={() => {
                  if (this.emailInput !== undefined) {
                    this.emailInput.focus();
                  }
                }}
                blurOnSubmit={false}
              />
            </View>
            <View style={styles.SectionStyle}>
              <TextInput
                value={this.state.userEmail}
                style={styles.inputStyle}
                onChangeText={(userEmail: string) =>
                  this.setState({userEmail: userEmail.trim()})
                }
                ref={(input: TextInput) => {
                  this.emailInput = input;
                }}
                onSubmitEditing={() => {
                  if (this.password1Input !== undefined) {
                    this.password1Input.focus();
                  }
                }}
                underlineColorAndroid="#F6F6F7"
                placeholder="Wprowadź e-mail"
                placeholderTextColor="#F6F6F7"
                keyboardType="email-address"
                returnKeyType="next"
                blurOnSubmit={false}
              />
            </View>
            <View style={styles.SectionStyle}>
              <TextInput
                value={this.state.userPassword}
                style={styles.inputStyle}
                onChangeText={(password1) =>
                  this.setState({userPassword: password1.trim()})
                }
                ref={(input: TextInput) => {
                  this.password1Input = input;
                }}
                onSubmitEditing={() => {
                  if (this.password2Input !== undefined) {
                    this.password2Input.focus();
                  }
                }}
                underlineColorAndroid="#F6F6F7"
                placeholder="Wprowadź hasło"
                placeholderTextColor="#F6F6F7"
                keyboardType="default"
                blurOnSubmit={false}
                secureTextEntry={true}
              />
            </View>
            <View style={styles.SectionStyle}>
              <TextInput
                style={styles.inputStyle}
                onChangeText={(password2) =>
                  this.setState({userPassword2: password2.trim()})
                }
                ref={(input: TextInput) => {
                  this.password2Input = input;
                }}
                value={this.state.userPassword2}
                underlineColorAndroid="#FFFFFF"
                placeholder="Wprowadź hasło ponownie"
                placeholderTextColor="#F6F6F7"
                keyboardType="default"
                onSubmitEditing={Keyboard.dismiss}
                blurOnSubmit={false}
                secureTextEntry={true}
              />
            </View>
            {this.state.errorText !== '' ? (
              <Text style={styles.errorTextStyle}>
                {' '}
                {this.state.errorText}{' '}
              </Text>
            ) : null}
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={this.handleSubmitButton}>
              <Text style={styles.buttonTextStyle}>Zarejestruj się</Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    );
  }

  handleSubmitButton = () => {
    if (this.validateInputs()) {
      this.setErrorText('');
      this.setLoading(true);
      axios
        .post(REGISTER_URL, {
          nick: this.state.nick,
          password: this.state.userPassword,
          email: this.state.userEmail,
        })
        .then((_response: AxiosResponse) => {
          this.setState({
            registered: true,
          });
          this.setLoading(false);
        })
        .catch((_error) => {
          this.setErrorText('Użytkownik z takim nickiem już istnieje!');
          this.setLoading(false);
        });
    }
  };

  validateInputs = () => {
    if (this.state.nick.trim() === '') {
      this.setErrorText('Pole nick nie może być puste!');
      return false;
    }
    if (this.state.userEmail.trim() === '') {
      this.setErrorText('Pole e-mail nie może być puste!');
      return false;
    }
    if (this.state.userPassword.trim().length < 8) {
      this.setErrorText('Hasło musie mieć przynajmniej osiem znaków!');
      return false;
    }
    if (this.state.userPassword.trim() !== this.state.userPassword2.trim()) {
      this.setErrorText('Podane hasła się różnia!');
      return false;
    }
    return true;
  };

  setLoading = (newLoadingValue: boolean) => {
    this.setState({
      loading: newLoadingValue,
    });
  };

  setErrorText = (newErrorText: string) => {
    this.setState({
      errorText: newErrorText,
    });
  };
}

const styles = StyleSheet.create({
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    marginTop: 20,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },
  buttonStyle: {
    backgroundColor: '#7DE24E',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#7DE24E',
    height: 40,
    alignItems: 'center',
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 20,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: 'white',
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 30,
    borderColor: 'white',
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
  successTextStyle: {
    color: 'white',
    textAlign: 'center',
    fontSize: 18,
    padding: 30,
  },
});

const mapStateToProps = (state: any) => {
  const {decimeState} = state;
  return {decimeState};
};

export default connect(mapStateToProps)(RegisterScreen);
