/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  KeyboardAvoidingView,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import Loader from '../loader/loader';
import {DecimeState} from '../../redux-saga/decime-reducer';
import axios, {AxiosResponse} from 'axios';
import {LOGIN_URL} from '../../api';
import {LOGIN_SUCCESS_ACTION_TYPE} from '../../redux-saga/actions/login-action';

interface Props {
  navigation: any;
  dispatch(action: any): void;
  decimeState: DecimeState;
}

interface State {
  nick: string;
  userPassword: string;
  loading: boolean;
  errorText: string;
}

class LoginScreen extends Component<Props, State> {
  secondTextInput: TextInput | undefined;
  constructor(props: Props) {
    super(props);
    console.log(props);
    this.state = {
      nick: '',
      userPassword: '',
      loading: false,
      errorText: '',
    };
  }

  componentDidMount() {
    this.checkIfLoggedIn();
  }

  componentDidUpdate() {
    this.checkIfLoggedIn();
  }

  render() {
    return (
      <View style={styles.mainBody}>
        <Loader loading={this.state.loading} />
        <ScrollView keyboardShouldPersistTaps="handled">
          <View style={{marginTop: 100}}>
            <KeyboardAvoidingView enabled>
              <View style={{alignItems: 'center'}}>
                <Image
                  source={require('../images/react.png')}
                  style={styles.image}
                />
              </View>
              <View style={styles.SectionStyle}>
                <TextInput
                  style={styles.inputStyle}
                  value={this.state.nick}
                  onChangeText={(nick: string) =>
                    this.setState({nick: nick.trim()})
                  }
                  underlineColorAndroid="#FFFFFF"
                  placeholder="Wprowadź nazwę użytkownika"
                  placeholderTextColor="#F6F6F7"
                  autoCapitalize="none"
                  keyboardType="email-address"
                  returnKeyType="next"
                  onSubmitEditing={() => {
                    if (this.secondTextInput !== undefined) {
                      this.secondTextInput.focus();
                    }
                  }}
                  blurOnSubmit={false}
                />
              </View>
              <View style={styles.SectionStyle}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(userPassword: string) =>
                    this.setState({userPassword: userPassword.trim()})
                  }
                  ref={(input: TextInput) => {
                    this.secondTextInput = input;
                  }}
                  underlineColorAndroid="#FFFFFF"
                  placeholder="Wprowadź hasło"
                  placeholderTextColor="#F6F6F7"
                  keyboardType="default"
                  onSubmitEditing={Keyboard.dismiss}
                  blurOnSubmit={false}
                  secureTextEntry={true}
                />
              </View>
              {this.state.errorText !== '' ? (
                <Text style={styles.errorTextStyle}>
                  {' '}
                  {this.state.errorText}{' '}
                </Text>
              ) : null}
              <TouchableOpacity
                style={styles.buttonStyle}
                activeOpacity={0.5}
                onPress={this.handleSubmitPress}>
                <Text style={styles.buttonTextStyle}>Zaloguj się</Text>
              </TouchableOpacity>
              <Text
                style={styles.registerTextStyle}
                onPress={() =>
                  this.props.navigation.navigate('RegisterScreen')
                }>
                Nie masz konta ? Zarejestruj się
              </Text>
            </KeyboardAvoidingView>
          </View>
        </ScrollView>
      </View>
    );
  }

  checkIfLoggedIn = () => {
    if (this.props.decimeState.auth.token !== null) {
      this.props.navigation.navigate('DrawerNavigationRoutes');
    }
  };

  handleSubmitPress = () => {
    this.setErrorText('');
    this.setLoading(true);
    axios
      .post(LOGIN_URL, {
        nick: this.state.nick,
        password: this.state.userPassword,
      })
      .then((response: AxiosResponse) => {
        this.setLoading(false);
        const token: string = response.data;
        this.props.dispatch({
          type: LOGIN_SUCCESS_ACTION_TYPE,
          payload: {
            token: token,
            userName: this.state.nick,
          },
        });
      })
      .catch((_error) => {
        this.setErrorText('Błędna nazwa użytkownika lub hasło!');
        this.setLoading(false);
      });
  };

  setLoading = (newLoadingValue: boolean) => {
    this.setState({
      loading: newLoadingValue,
    });
  };

  setErrorText = (newErrorText: string) => {
    this.setState({
      errorText: newErrorText,
    });
  };
}

const styles = StyleSheet.create({
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#307ecc',
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    marginTop: 20,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },
  buttonStyle: {
    backgroundColor: '#7DE24E',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#7DE24E',
    height: 40,
    alignItems: 'center',
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 20,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: 'white',
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 30,
    borderColor: 'white',
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
  image: {
    width: '50%',
    height: 100,
    resizeMode: 'contain',
    margin: 30,
  },
});

const mapStateToProps = (state: any) => {
  const {decimeState} = state;
  return {decimeState};
};

export default connect(mapStateToProps)(LoginScreen);
