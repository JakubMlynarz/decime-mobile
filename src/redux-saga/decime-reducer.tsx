import {combineReducers} from 'redux';
import {Action} from './actions/action';
import {
  LOGOUT_ACTION_TYPE,
  LOGIN_SUCCESS_ACTION_TYPE,
} from './actions/login-action';

interface AuthData {
  token: string | null;
  userName: string;
}

export interface DecimeState {
  auth: AuthData;
}

function getInitialState(): DecimeState {
  return {
    auth: {
      token: null,
      userName: '',
    },
  };
}

function decimeReducer(state: DecimeState = getInitialState(), action: Action) {
  if (
    !action.type.startsWith('persist') &&
    !action.type.startsWith('@@redux')
  ) {
    console.log(action);
  }
  if (action.type === LOGIN_SUCCESS_ACTION_TYPE) {
    state.auth.token = action.payload.token;
    state.auth.userName = action.payload.userName;
    return Object.assign({}, state);
  }
  if (action.type === LOGOUT_ACTION_TYPE) {
    state.auth.token = null;
    state.auth.userName = '';
    return Object.assign({}, state);
  }
  return state;
}

export default combineReducers({
  decimeState: decimeReducer,
});
