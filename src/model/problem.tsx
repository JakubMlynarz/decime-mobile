interface PromotionAgreement {
  id: number;
  creditsAmount: number;
}

interface AnswerInstance {
  id: number;
  userNick: string | null;
  createdAt: string | null;
  comment: string | null;
}

export interface PossibleAnswer {
  id: number;
  answer: string;
  count: number;
  answerInstances: AnswerInstance[];
}

export interface Problem {
  id: number;
  possibleAnswers: PossibleAnswer[];
  creationDate: string | null;
  promotionAgreement: PromotionAgreement | null;
  problemQuestion: string | null;
  userNick: string | null;
}
