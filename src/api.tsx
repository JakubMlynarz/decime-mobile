/* eslint-disable prettier/prettier */
export const API_BASE_URL: string = 'http://jakubmlynarz.pl:8001';
export const LOGIN_URL = API_BASE_URL + '/user/login';
export const REGISTER_URL = API_BASE_URL + '/user/register';
export const GET_PROBLEMS_URL = API_BASE_URL + '/api/problems/feed';
export const GET_PROBLEM_URL = API_BASE_URL + '/api/problems/{id}';
export const VOTE_PROBLEM_URL = API_BASE_URL + '/api/problems/vote';
export const ADD_PROBLEM_URL = API_BASE_URL + '/api/problems/add';
export const GET_PROBLEMS_CATEGORIES = API_BASE_URL + '/api/problems/categories';
export const GET_PROBLEMS_IN_CATEGORY = API_BASE_URL + '/api/problems/category/{category}';
export const GET_COMMENTS_FOR_PROBLEM = API_BASE_URL + '/api/problems/comments/{id}';
export const ADD_COMMENT = API_BASE_URL + '/api/problems/comments';
